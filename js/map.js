// JavaScript Document

window.onload = function () {

   	var latlng = new google.maps.LatLng(52.127069, -106.673465);

   	var styles = [
		{"featureType":"water","elementType":"geometry","stylers":[
			{"color":"#bfbfbf"},
			{"lightness":0}
			]
		},
		{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f0f0f0"},{"lightness":0}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#929292"},{"lightness":0}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#bfbfbf"},{"lightness":0},{"weight":1}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#bfbfbf"},{"lightness":0}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#bfbfbf"},{"lightness":0}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#e3e3e3"},{"lightness":0}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"color":"#484848"},{"lightness":0}]},{"elementType":"labels.text.fill","stylers":[{"saturation":0},{"color":"#4c4b48"},{"lightness":0}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f0f0f0"},{"lightness":0}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#bfbfbf"},{"lightness":0}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#f0f0f0"},{"lightness":0},{"weight":1.2}]}]


	var isDraggable = $(document).width() > 5000 ? true : false; 
	var mapMarker = $(document).width() > 768 ? true : false; 


   	var myOptions = {
		draggable: isDraggable, 
		zoom: 16,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		//disableDefaultUI: true,
		//styles: styles,
		scrollwheel: isDraggable
   	};
   
   	map = new google.maps.Map(document.getElementById('map'), myOptions);
   
   	if(mapMarker == false){
   
		var marker = new google.maps.Marker({
			position: latlng,
			icon: 'images/marker_ph.png',
			map: map,
			title:"Av Shuttle"
		});
	
   }else{
	   var marker = new google.maps.Marker({
			position: latlng,
			icon: 'images/map_marker.png',
			map: map,
			title:"Av Shuttle"
		});
   }
	
	

}



